//[SECTION] React Components
	import { useState, useContext } from 'react';
	import { Navbar, Nav } from 'react-bootstrap';
	import UserContext from '../UserContext';

//[SECTION] REACT-ROUTER
	import { Link } from 'react-router-dom';


export default function AppNavbar() {
	//Store the user information
	const { user } = useContext(UserContext);

	//getItem gets the key property in the local storage
	// const [ user, setUser ] = useState(localStorage.getItem('email'))
	// console.log(user);

	return(
		<Navbar bg="dark" expand="lg" variant="dark" className="mb-5">
			<Navbar.Brand className="ms-3">Zuitt</Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav"/>
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="ms-auto">
					<Nav.Link as={Link} to="/">Home</Nav.Link>
					<Nav.Link as={Link} to="/courses">Courses</Nav.Link>

					{(user.accessToken !== null) ?
						<Nav.Link as={Link} to="/logout">Logout</Nav.Link>
						:
						<>
							<Nav.Link as={Link} to="/login">Login</Nav.Link>
							<Nav.Link as={Link} to="/register">Register</Nav.Link>
						</>
					}
				</Nav>
			</Navbar.Collapse>
		</Navbar>
		)
}