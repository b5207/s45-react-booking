//[SECTION] React Components
	import { useState, useEffect } from 'react';
	import { Card, Button } from 'react-bootstrap';
	import PropTypes from 'prop-types';
	import { Link } from 'react-router-dom';


//Destructure the object {courseProp}
export default function CourseCard({courseProp}){
	//Checks if the data was successfully passed.
	//console.log(props)

	//Deconstruct the Course Properties into their own variables.
	const { _id, name, description, price } = courseProp;

	//Use the state hook for this component to be able to store its value.
	//[States] are used to keep track of info related to individual components.


	//[SYNTAX]
		//const [currentValue(getter), updatedValue(setter)] = useState(initialGetterValue)
		//As per documentation we put "set" before the word to indicate that it is the setter.
		//const [ count, setCount ] = useState(0);

		//Set the available seats for enrollees to 30 only.
		//const [ slot, setSlot ] = useState(30);

		//For the automatic enabling/disabling of button once slots have been filled.
		//const [ isOpen, setIsOpen ] = useState(true);

		/*useEffect(() => {
			if(slot === 0){
				setIsOpen(false);
			}
		}, [slot])

		function enroll() {
			if(slot > 0){
				setCount(count + 1);
				console.log('Enrollees: ' + count);
				setSlot(slot - 1);
				console.log('Slots Left: ' + count);
			} 
		}*/


	return(
		<Card className="mt-3">
			<Card.Body>
				<Card.Title>{ name }</Card.Title>

				<Card.Subtitle>Description:</Card.Subtitle>
				<Card.Text>{ description }</Card.Text>

				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>PHP { price }</Card.Text>

				<Button variant="primary" as={ Link } to={`/courses/${_id}`}>Details</Button>
					
			</Card.Body>
		</Card>
		)
}


//Check if the CourseCard component is getting the correct prop types.
	//Proptype is used for validating info passed to a component and is used to help developers ensure the correct info is passed from one component to another.
	CourseCard.propTypes = {
		//shape() method is used to check if a prop object has the specific shape of data types.

		courseProp: PropTypes.shape({
			//Define the properties and their expected types.
			name: PropTypes.string.isRequired,
			description: PropTypes.string.isRequired,
			price: PropTypes.number.isRequired
		})
	}