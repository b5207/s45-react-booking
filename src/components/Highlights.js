//[SECTION] React Bootstrap Components
	import { Row, Col, Card } from 'react-bootstrap';


export default function Highlights(){
	return(
		<Row>
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
					<Card.Body>
						<Card.Title>
							<h2>Learn From Home</h2>
						</Card.Title>

						<Card.Text>
							Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae excepturi dignissimos, error adipisci ullam nisi dolorem ipsum, nemo impedit modi in sequi saepe animi illum quam voluptatibus. Et quisquam, commodi ad. Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae excepturi dignissimos, error adipisci ullam nisi dolorem ipsum, nemo impedit modi in sequi saepe animi illum quam voluptatibus. Et quisquam, commodi ad.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
					<Card.Body>
						<Card.Title>
							<h2>Study Now, Pay Later</h2>
						</Card.Title>

						<Card.Text>
							Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae excepturi dignissimos, error adipisci ullam nisi dolorem ipsum, nemo impedit modi in sequi saepe animi illum quam voluptatibus. Et quisquam, commodi ad.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
					<Card.Body>
						<Card.Title>
							<h2>Be Part of Our Community</h2>
						</Card.Title>

						<Card.Text>
							Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae excepturi dignissimos, error adipisci ullam nisi dolorem ipsum, nemo impedit modi in sequi saepe animi illum quam voluptatibus. Et quisquam, commodi ad.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>

		</Row>
		)
}