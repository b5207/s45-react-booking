//[SECTION] React Components
	import React from 'react';

//The UserContext creates a context object.
//As the name suggests, it is a data type of an object that can be used to store information and shared with other components within the application.
//We use this to avoid the use of prop drilling.
	const UserContext = React.createContext();

//Provider component -> this allows other components to consume/use the context object and supply the neccessary info needed in the context object.
export const UserProvider = UserContext.Provider;

export default UserContext;