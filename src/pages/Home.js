//[SECTION] React Components
  import Banner from '../components/Banner';
  import Highlights from '../components/Highlights';


export default function Home() {
  return(
    <>
      <Banner name="Casi" age={18}/>
      <Highlights />
      
    </>
    )
}