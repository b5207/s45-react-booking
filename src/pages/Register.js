//[SECTION] React Components
	import { useState, useEffect, useContext } from 'react';
	import { Form, Button } from 'react-bootstrap';
	import Swal from 'sweetalert2';
	import UserContext from '../UserContext';
	import { Navigate } from 'react-router-dom';


export default function Register(){
	const { user } = useContext(UserContext);

	//[State hooks] to store the values of the input fields.
	const [ email, setEmail ] = useState('');
	const [ password, setPassword ] = useState('');
	const [ verifyPassword, setVerifyPassword ] = useState('');

	//[State] for the enable/disable button
	const [ isActive, setIsActive ] = useState(true);

	useEffect(() => {
		//Validation to enable submit button.
		if((email !== '' && password !=='' && verifyPassword !=='') && (password === verifyPassword)){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [email, password, verifyPassword])


	function registerUser(e) {
		e.preventDefault();

		//Clear input fields
		setEmail('');
		setPassword('');
		setVerifyPassword('');

		Swal.fire({
			title: 'Success!',
			icon: 'success',
			text: 'You have registered a new account.'
		})
	}

	return (
		(user.accessToken !== null) ?
		<Navigate to="/courses" />
		:

		<Form onSubmit={e => registerUser(e)}>
			<h1>Register New Account</h1>
			<Form.Group className="mt-2">
				<Form.Label>Email Address</Form.Label>
				<Form.Control type="email" placeholder="Enter email here" required value={email} onChange={e => setEmail(e.target.value)}/>
				<Form.Text className="text-muted ps-3">
					*We'll never share your email with anyone else.
				</Form.Text>
			</Form.Group>

			<Form.Group className="mt-2">
				<Form.Label>Password</Form.Label>
				<Form.Control type="password" placeholder="Enter your password here" required 
				value={password} onChange={e => setPassword(e.target.value)}/>
			</Form.Group>

			<Form.Group className="mt-2">
				<Form.Label>Verify Password</Form.Label>
				<Form.Control type="password" placeholder="Verify your password" required 
				value={verifyPassword} onChange={e => setVerifyPassword(e.target.value)}/>
			</Form.Group>

			{ isActive ? 
				<Button className="mt-3" variant="primary" type="submit">Submit</Button>
				:
				<Button className="mt-3" variant="primary" type="submit" disabled>Submit</Button>
			}
			
		</Form>
		)
}