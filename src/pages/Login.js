//[SECTION] React Components
	import { useState, useEffect, useContext } from 'react';
	import { Form, Button } from 'react-bootstrap';
	import Swal from 'sweetalert2';
	import UserContext from '../UserContext';
	import { Navigate, useNavigate } from 'react-router-dom';
	

export default function Login() {

	/*NOTE for fetch()
		- It is a method in JS which allows to send a request to an api and process its response.

	fetch('url', {optional object})
		-url from the API (http://localhost:4000/users/login) (https://heroku.com/users/login)
		-{optional objects} are objects that contain additional info about our requests such as method, body, and headers: content-type, authorization

	//Getting a response is usually a two-stage process.	
	.then(response => response.json()) --> to parse the response as JSON.
	.then(actualData => console.log(actualData)) --> to process
	*/

	const navigate = useNavigate();

	//Deconstruct the User Context object and its properties to use for user validation and to get the email coming from the login.
	const { user, setUser } = useContext(UserContext);

	const [ email, setEmail ] = useState('');
	const [ password, setPassword ] = useState('');

	//BUTTON
	const [ isActive, setIsActive ] = useState(true);

	useEffect(() => {
		if(email !== '' && password !== ''){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [email, password])

	function authentication(e){
		e.preventDefault();

		fetch('https://cube-course-builder.herokuapp.com/users/login', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(response => response.json())
		.then(data => {
			console.log(data)
			if(data.accessToken !== undefined){
				localStorage.setItem('accessToken', data.accessToken);
				setUser({
					accessToken: data.accessToken
				})

				Swal.fire({
					title: 'Success',
					icon: 'success',
					text: 'You are now logged in. Happy enrolling!'
				})

				//Get user's details from our token
				fetch('http://localhost:4000/users/details', {
					headers: {
						Authorization: `Bearer ${data.accessToken}`
					}
				})
				.then(res => res.json())
				.then(data => {
					console.log(data)

					if(data.isAdmin === true) {
						localStorage.setItem('isAdmin', data.isAdmin)

						setUser({
							isAdmin: data.isAdmin
						})

						//Push to the /adminDashboard
						navigate('/courses')

					} else {
						//If not an admin, push to the '/' (homepage)
						navigate('/')
					}

				})

			} else {
				Swal.fire({
					title: 'Oooopsss',
					icon: 'error',
					text: 'Something went wrong. Check your email or password.'
				})
			}

			setEmail('');
			setPassword('');
		})
	}


	return (
		(user.accessToken !== null) ?
		<Navigate to="/courses" />
		:

	<Form onSubmit={e => authentication(e)}>
		<h1>Login</h1>
		<Form.Group className="mt-2">
			<Form.Label>Email Address</Form.Label>
			<Form.Control type="email" placeholder="Enter email here" required 
			value={email} onChange={e => setEmail(e.target.value)} />
			<Form.Text className="text-muted ps-3">
				*We'll never share your email with anyone else.
			</Form.Text>
		</Form.Group>

		<Form.Group className="mt-2">
			<Form.Label>Password</Form.Label>
			<Form.Control type="password" placeholder="Enter your password here" required 
			value={password} onChange={e => setPassword(e.target.value)} />
		</Form.Group>

		{ isActive ? 
			<Button className="mt-3" variant="primary" type="submit">Submit</Button>
			:
			<Button className="mt-3" variant="primary" type="submit" disabled>Submit</Button>
		}
	</Form>
	)
}